package com.pietro.materialdesign.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.pietro.materialdesign.DAO.Car;
import com.pietro.materialdesign.interfaces.RecyclerViewOnClickListenerHack;
import com.pietro.materialdesigntoolbar.R;

import java.util.List;

/**
 * Created by pietrogirardi on 7/25/15.
 */
public class CarAdapter extends RecyclerView.Adapter<CarAdapter.MyViewHolder>{
    private List<Car> listCar;
    private LayoutInflater lInflater;
    RecyclerViewOnClickListenerHack recyclerViewOnClickListenerHack;

    public CarAdapter(Context context, List<Car> listCar) {
        this.listCar = listCar;
        lInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = lInflater.inflate(R.layout.item_car, viewGroup, false);
        MyViewHolder mvh = new MyViewHolder(view);
        return mvh;
    }

    @Override
    public void onBindViewHolder(MyViewHolder myViewHolder, int position) {
        myViewHolder.imgCar.setImageResource(listCar.get(position).getPhoto());
        myViewHolder.tvBrand.setText(listCar.get(position).getBrand());
        myViewHolder.tvModel.setText(listCar.get(position).getModel());

    }

    @Override
    public int getItemCount() {
        return listCar.size();
    }

    public void addCar(Car car, int position){
        listCar.add(car);
        notifyItemInserted(position);
    }

    public void removeCar(Car car){
        listCar.remove(car);
    }

    public void removeCar(int position){
        listCar.remove(position);
        notifyItemRemoved(position);
    }

    public void setRecyclerViewOnClickListenerHack(RecyclerViewOnClickListenerHack rv){
        recyclerViewOnClickListenerHack = rv;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        ImageView imgCar;
        TextView tvModel;
        TextView tvBrand;

        public MyViewHolder(View itemView) {
            super(itemView);

            imgCar = (ImageView) itemView.findViewById(R.id.iv_car);
            tvModel = (TextView) itemView.findViewById(R.id.tv_model);
            tvBrand = (TextView) itemView.findViewById(R.id.tv_brand);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if(recyclerViewOnClickListenerHack != null){
                recyclerViewOnClickListenerHack.onClickListener(v, getPosition());
            }
        }
    }
}
